[[_TOC_]]

----

# Intro 

Conda permet de gérer plusieurs **environnements** de développement et d'analyse, notamment pour python et R.

Il permet d'avoir **en parallèle plusieurs versions** pour **R** (3.5, 3.6, ...) ainsi que des librairies associées à chaque environnement ; de même pour **python** (2.7, 3.5, 3.6, 3.7, ...) et ses modules.

Les programmes et librairies sont disponibles dans différents dépôts ou **channels**. Les principaux qui nous intéressent : conda-forge et bioconda

Cela permet donc de gérer et personnaliser les logiciels et librairies associées avec leur version au niveau de chaque utilisateur.

# Installation

Est-ce installé ?
``` bash
dnf list conda
```
```
Last metadata expiration check: 0:00:27 ago on Wed 22 Jan 2020 07:56:36 AM CET.
Installed Packages
conda.noarch                  4.6.14-1.fc30                     @updates
```

oui.

Pour l'installer en tant qu'administrateur
``` bash
sudo dnf install conda
```

Information sur l'installation
```bash
conda info
```

# Environnement

**Remarque :** Toutes les commandes `conda` sont à exécuter **en tant qu'utilisateur** et **surtout PAS administrateur/root**.

Utilisation de `conda` qui permet de créer différents environnement avec différentes versions de python, de R, ou de leurs librairies (parfois certaines libs sont en conflits ou ne fonctionnent pas si certaines autres sont installées).

## Création d'un environnement : `conda create`

Création d'un nouvel environnement (qui utilise la version 3.7 de python)
```bash 
conda create --name bbs python=3.7
```

ou bien en deux étapes (création de l'environnement puis installation de python 3.7)
``` bash
conda create --name bbs
```

Liste des environnements
``` bash
conda env list
```

## Changement d'environnement : `conda activate/deactivate`

``` bash
conda activate bbs
```

**Remarque :** En activant un environnement, il est ajouté à celui en cours (et le masque). Pour gérer un environnement "propre"/indépendant, il peut être conseillé de d'abord désactiver toute la pile d'environnements activés (par exemple, pour ne pas remplacer la version de R ou python) :

``` bash
conda deactivate
```

A répéter tant qu'on n'est pas sorti de tous les environnements.

**Remarque :** Pour utiliser RStudio (ou R ou python ou ...) dans cet environnement, il faut activer l'environnement et **lancer Rstudio en ligne de commandes** :
``` bash
conda activate bbs
rstudio
```

## Suppression d'un environnement : `conda remove`

``` bash
conda remove --name myenv --all
```

## Clonage d'un environnement : `conda clone`

``` bash
conda create --name newly_created_env --clone existing_env
```

**Remarque :** très utile si on veut tester et éviter de casser ce qui fonctionne

# Ajout de dépôts : `conda config --add channels`

``` bash
conda config --add channels conda-forge 
conda config --add channels bioconda 
```

# Gestion des packages : `conda search/install/uninstall/upgrade`

Liste de packages installés dans un environnement :
``` bash
conda list -n bbs
```

## Installation d'un package : `conda install`

Installation d'un logiciel ou package (en précisant sa version)
``` bash
conda install r-base=3.5
```

**Remarques :**

  * avant de valider l'installation, il faut vérifier qu'elle n'implique pas des mises à jours pouvant "casser" l'environnement (downgrade ou upgrade d'autres packages).
  * les noms/identifiants des packages sont en minuscules
  * les librairies R ont en général le préfixe `r-`, exemple : `r-tidyverse`
  * les librairies R-Bioconductor on en général le préfixe `bioconductor-`, exemple : `bioconductor-ggtree`
* lies librairies Perl ... `perl-`

Test de la version de python
``` bash
python --version
```

Activation d'un environnement
``` bash
conda activate bbs
```

Test de la version de python
``` bash
python --version
```

Pour quitter l'environnement
``` bash
conda deactivate
```

== Recherche d'un package : `conda search` == 

``` bash
conda search biopython
```

# Exemple pour de la bioinfo

``` bash
conda config --add channels conda-forge 
conda config --add channels bioconda 
```

Environnement Python et R
``` bash
conda create --name bioinf python=3.7 r-base=3.6
```

``` bash
conda activate bioinf
```

Modules python
``` bash
conda install biopython igraph numpy pandas scipy scikit-learn matplotlib 
```

Librairies R
``` bash
conda install r-tidyverse r-ggally r-purrr r-igraph r-reticulate r-rmysql r-lattice  r-biocmanager r-devtools r-caTools r-rprojroot r-shiny r-phytools r-knitr r-dt r-kableextra
```

Librairies Bioconductor
``` bash
conda install bioconductor-rhtslib bioconductor-chipseq bioconductor-GenomicRanges bioconductor-edgeR bioconductor-genefilter bioconductor-geneplotter bioconductor-DESeq2 bioconductor-mixOmics bioconductor-STRINGdb
```

Librairies R/NGS supplémentaires
``` bash
conda install bioconductor-BSgenome  bioconductor-BSgenome.Dmelanogaster.UCSC.dm3 bioconductor-BSgenome.Dmelanogaster.UCSC.dm3.masked bioconductor-MotifDb bioconductor-seqLogo bioconductor-motifStack bioconductor-GenomicFeatures bioconductor-TxDb.Dmelanogaster.UCSC.dm3.ensGene bioconductor-Rqc bioconductor-pasillaBamSubset bioconductor-MMDiffBamSubset bioconductor-org.Dm.eg.db bioconductor-drosophila2.db bioconductor-drosophila2probe bioconductor-drosophila2cdf bioconductor-hom.Dm.inp.db bioconductor-GO.db bioconductor-biomaRt bioconductor-SRAdb bioconductor-GEOquery bioconductor-Gviz bioconductor-AnnotationHub
```

Quelques logiciels :

  * alignement multiple : `conda search mafft=7.455 --info`
  * visualisation de séquences/d'alignements : `conda search jalview=2.11.0 --info`
  * gestion de fichiers d'aide en ligne de commande : `conda install cheat`
  * client mysql/mariadb plus ergonomique : `conda install mycli`
