
[[_TOC_]]

# Processus

- Système d'exploitation (*Operating system* ou OS) correspond à l'environnement d'exécution d'un programme
- Processus = Instance d'un fichier de programme en cours d'exécution
    - exécution séquentielle des instructions du programme
    - possibilité de "sentir" son environnement
    - possibilité d'agir
- Entrées/Sorties
    - entrées : fichiers (et fichiers spéciaux de type capteurs), capteurs, clavier, souris, ... = périphériques
    - sorties : fichiers (et fichiers spéciaux), écran, son, thermostat du radiateur, frein, ... = périphériques
- Shell : interpréteur de commandes pour interagir avec l'OS avec un langage (`sh`, `bash`, `tcsh`, ...) plus ou moins élaboré
    - lancement de programmes
    - environnement (variables, fonctions, ...)
    - structures de données (une seule valeur, liste de valeurs, ...)
    - structures de contrôles (`if`, `for`, ...)


Les ordis de nos jours ont généralement un OS multi-tâches et multi-utilisateurs, et permettent donc l'exécution de plusieurs programmes en parallèle appartenants à plusieurs utilisateurs.

# bash

Nous allons utiliser `bash` qui est celui utilisé par défaut dans la salle ainsi que sur les machines du campus.

Le shell utilisé est un processus comme un autre. Pour afficher les processus en cours (au format long):
```bash
$guest ps -f
UID          PID    PPID  C STIME TTY          TIME CMD
barriot  3220108 2845569  0 Jan18 pts/8    00:00:00 bash
barriot  3722658 3220108  0 16:36 pts/8    00:00:00 ps -f
```

Ici, on peut voir que le processus `bash` appartient à l'utilisateur `barriot` et à l'identifiant `3220108` (PID pour Process ID).

L'autre processus `ps` appartient aussi à `barriot` avec un autre PID, son processus parent à pour identifiant `3220108` (PPID pour Parent Process ID) donc `bash` notre interpréteur de commandes.

`ps` est donc l'exécution d'un programme, mais où se trouve-t-il ?
```bash
$guest which ps
/usr/bin/ps
```

A qui appartient ce fichier et quelles sont les permissions sur celui-ci :
```
$guest ls -lh /usr/bin/ps
-rwxr-xr-x 1 root root 134K Aug 15 02:00 /usr/bin/ps
 │  │  │     │    │    │    └  date et heure de dernière modification
 │  │  │     │    |    └ taille
 │  │  │     │    └ groupe
 │  │  │     └ propriétaire
 │  │  └ permissions pour "other"
 │  └ permissions pour le groupe root
 └ permissions pour l'utilisateur root
```

Ici, l'utilisateur barriot, ni propriétaire ni du groupe root, a les droits *other* `r-x` et peut donc lire `r` et exécuter `x` le programme.


# Variables d'environnement

Au démarrage d'un processus *shell*, un certain nombre (relativement conséquent) de variables, de fonctions et d'alias sont définis.

Pour l'apprécier, la commande `set` (*built-in* bash) permet de positionner des variables/fonctions ;  appelée sans paramètre, elle les affiche :
```bash
$guest set | less
```

Certaines variables d'environnement peuvent être utiles pour les scripts et programmes :
- HOME: le répertoire racine de l'utilisateur ; ici, `/home/barriot`
- PWD: le répertoire actuel
- SHELL: l'interpréteur ; ici, `/bin/bash`
- EDITOR: souvent utilisées par les programmes pour invoqué l'éditeur de texte préférré par l'utilisateur (par exemple `vi` ou `nano`)


Pour les alias définis :
```bash
$guest alias
```

# PATH

Le texte l'invite de commande est découpé en mots pour être interpété/exécuté. Par défaut, le séparateur est souvent l'espace. 

Pour la commande `ps -f`, le premier mot est donc `ps` et va pouvoir être interprété comme une commande qui peut-être :

- une fonction
- un mot-clé du shell (for, if, alias, ...)
- un programme

Comme `ps` n'est ni une fonction (mais on pourrait en définir une) ni un mot-clé, le fichier de programme est recherché et trouvé dans `/usr/bin`.

Les répertoires et l'ordre dans lequel les programmes sont recherché sont définis dans la variable d'environnement `PATH` :
```bash
$guest echo $PATH
```

Lors de l'interprétation d'une ligne de commande, les variables sont remplacées par leur valeur (sauf si on les a mis entre simple quotes `'`) :
```bash
echo '$PATH'
```

Pour positionner une variable, on omet le `$` (attention pas d'espace avant et après le `=`) :
```bash
$guest oldPATH=$PATH

```

Cette variable n'est définie que dans ce shell. Pour s'en assurer, ouvrir un autre terminal et faire un `echo $oldPATH` puis fermer ce terminal.

De même, dans le terminal où `oldPATH` est défini, si on lance un autre shell (`bash` + `entrée`), ce nouveau shell/processus n'aura pas la variable `oldPATH` définie. **Vérifiez-le** puis quitter ce processus (commande `exit` à n'effectuer qu'une seule fois). De retour dans le shell parent, la variable `oldPATH` devrait toujours exister.

Pour qu'un processus hérite d'une variable d'environnement, il faut l'exporter :
```bash
$guest export oldPATH=$PATH
$guest bash
$guest echo $oldPATH
```

Pour savoir quelle commande est lancée :
```bash
$guest which ls
$guest which ssh
```

Ainsi, si la variable `PATH` n'est pas définie, le shell devrait très mal se comporter :
```bash
$guest PATH=
$guest whoami
$guest whereis ls
```

Pour revenir à la valeur intiale :
```bash
$guest PATH=$oldPATH
```



**Conclusion :** Pour qu'un programme puisse être exécuté en ligne de commande, il faut qu'il se trouve dans un des répertoires de la variable PATH **et** qu'il ait les permissions d'exécution pour l'utilisateur qui cherche à le lancer.


## LD_LIBRARY_PATH

Une autre variable utile à connaître, `LD_LIBRARY_PATH`, correspond aux répertoires dans lesquels sont recherchées les librairies partagées (shared object `.so` pour linux, et `.DLL` chez microsoft). Pour interroger les librairies utilisées par un exécutable, comme par exemple bash :
```bash
$guest ldd /bin/bash
```

Si une librairie n'est pas trouvée, soit le programme ne démarre pas, soit il plantera lorsqu'une des fonctions de la librairie sera appelée. Parfois, un logiciel stocke ses librairies dans un de ses répertoires et il est nécessaire d'ajouter ce répertoire dans la variable LD_LIBRARY_PATH pour le que programme puisse les trouver. 

# Entrées/Sorties, pipe, redirection

Au lancement, un processus a
- `stdin`: une entrée standard ayant le descripteur de fichier *fd* 0. Ceci permet au processus de lire du contenu passé par l'utilisateur.
- `stdout`: une sortie standard ayant le *fd* 1. Par défaut, c'est le terminal ; là où s'affiche les *print*.
- `stderr`: une sortie d'erreurs ayant le *fd* 2. Par défaut, c'est aussi le terminal. Elle sert à affichier les messages d'erreurs. Ces 2 sorties permettent de faire le tri entre les sorties du programmes et les messages d'erreurs.

Ce mécanisme permet de **rediriger** la sortie (`stdout` par défaut) d'un programme vers l'entrée (`stdin` par défault) d'un autre avec un *pipe* `|` :
```bash
$guest mount | awk -F' ' '{ printf "%s\t%s\n",$1,$3; }' | column -t | grep -E '^/dev/|^.*:|encfs' | sort
```

Les autres redirections possibles concernent les fichiers : on peut rediriger les sorties **stdout** et/ou **stderr** d'un processus vers des fichiers :
- `>` écrase/remplace le fichier s'il existait auparavant
- `>>` ajoute à la fin du fichier s'il existait

Pour rediriger `stdout` seulement, on utilisera `1>`. Exemple: `grep somestring in_this_file 1> grep.stdout`

Pour rediriger `stderr` seulement, on utilisera `2>`. Exemple: `grep somestring in_this_file 2> grep.stderr`

Pour rediriger `stdout` et `stderr` : `grep somestring in_this_file 1> grep.stdout 2> grep.stderr`

Pour rediriger `stderr` sur `stdout` : `grep somestring in_this_file 2>&1` ; l'ordre est important, d'abord vers un fichier puis `stderr` sur `stdout` : `grep somestring in_this_file > grep.stdouterr 2>&1` ; avec `bash`, on peut faire `grep somestring in_this_file &> grep.output`

Pour se débarrasser des messages d'erreur : `grep -R bash /bin 2> /dev/null`

# Chemins absolus ou relatifs
 
2 manières de spécifier l'emplacemet d'un fichier/répertoire :

- `chemin absolu` : commence par `/` la racine du système de fichier, exemple `/usr/bin/ps`

- `chemin relatif` : Ne commence PAS par `/`, et donc dépend de là où on se trouve dans l'arborescence de répertroire : `.` 

**Chemin relatif.** Le répertoire dans lequel on se trouve est désigné par `.`, le répertoire parent par `..`, les sous-répertoires par leur nom.

Exemple si l'on se trouve dans le répertoire `scripts` d'un projet ayant l'arobresence suivante :
```
 ┆
 └ awesome_project
   └┬ bin
    ├ data
    ├ doc
    ├ lib
    ├ scripts (ici)
    └ src
```

Pour accéder au répertoire data en étantdans le répertoire `scripts`, il faudra indiquer `../data`, indépendamment de là où se trouve `awesome_project`.

# Gestion des processus

Vue plus haut, la commande `ps` permet de lister les processus en cours d'exécution ; avec des options que j'utilise souvent pour avoir l'*arborescence* des processus :
```bash
$guest ps faux | less
```

D'autres utilitaires existent mais en sont pas toujours installés : `top`, `htop`, `glances`.


## premier/arrière plan, signaux

Par défaut, lors du lancement d'une commande, son exécution se fait au premier plan (*foreground*) et le shell attend qu'elle se termine avant de vous rendre la main. Lors du lancement d'une commande, on peut le faire directement en arrière plan (*background*) en ajoutant `&`, par exemple pour ouvrir un éditeur de texte, pour pouvoir lancer d'autres commandes par la suite. 

Un processus peut-être dans différents états : 
- *Running* sur un processeur, 
- *Sleeping* en attente d'un processeur pour poursuivre l'exécution,
- *Stopped* en pause, 
- *Zombie*, ...

Dans le shell, on peut terminer un processus en cours d'exécution au premier plan avec `Ctrl-C` ou le mettre en pause avec `Ctrl-Z`.
```bash
$guest glxgears
Ctrl-C

$guest geany
Ctrl-Z
```

Dans le 2ème cas, le processus est en pause et ne peut plus rien faire (ni écouter). on peut alors le remettre au premier plan avec `fg` ou le passer en arrière plan `bg`

Si plusieurs processus son en pause, on peut spécifier le n° du job :
```bash
$guest geany 
Ctrl-Z
$guest glxgears
Ctrl-Z
$guest jobs
[1]-  Stopped                 glxgears
[2]+  Stopped                 geany
$guest fg 1
```

`Ctrl-C` et `Ctrl-Z` envoie des signaux au processus ou au système pour gérer leur exécution.
Liste des signaux :
```bash
$guest kill -l
 1) SIGHUP	     2) SIGINT	     3) SIGQUIT	     4) SIGILL	     5) SIGTRAP
 6) SIGABRT	     7) SIGBUS	     8) SIGFPE	     9) SIGKILL	    10) SIGUSR1
11) SIGSEGV	    12) SIGUSR2	    13) SIGPIPE	    14) SIGALRM	    15) SIGTERM
16) SIGSTKFLT	17) SIGCHLD	    18) SIGCONT	    19) SIGSTOP	    20) SIGTSTP
21) SIGTTIN	    22) SIGTTOU	    23) SIGURG	    24) SIGXCPU	    25) SIGXFSZ
26) SIGVTALRM	27) SIGPROF	    28) SIGWINCH	29) SIGIO	    30) SIGPWR
31) SIGSYS	    34) SIGRTMIN	35) SIGRTMIN+1	36) SIGRTMIN+2	37) SIGRTMIN+3
38) SIGRTMIN+4	39) SIGRTMIN+5	40) SIGRTMIN+6	41) SIGRTMIN+7	42) SIGRTMIN+8
43) SIGRTMIN+9	44) SIGRTMIN+10	45) SIGRTMIN+11	46) SIGRTMIN+12	47) SIGRTMIN+13
48) SIGRTMIN+14	49) SIGRTMIN+15	50) SIGRTMAX-14	51) SIGRTMAX-13	52) SIGRTMAX-12
53) SIGRTMAX-11	54) SIGRTMAX-10	55) SIGRTMAX-9	56) SIGRTMAX-8	57) SIGRTMAX-7
58) SIGRTMAX-6	59) SIGRTMAX-5	60) SIGRTMAX-4	61) SIGRTMAX-3	62) SIGRTMAX-2
63) SIGRTMAX-1	64) SIGRTMAX	
```

Les plus courants sont SIGHUP/SIGINT/SIGQUIT (à vérifier, on demande au processus de terminer mais il ne répond pas toujours), SIGKILL (on demande au système de tuer le processus), SIGSTOP (on demande de le mettre en pause), SIGCONT (on demande d'arrêter la pause). Les processus sont libres d'écouter ou pas ces signaux.

## arboresence de processus

Au lancement d'une commande, le processus est relié à son processus parent. Si le processus parent termine, tous les processus qu'il a lancé terminent aussi. Cela peut être ennuyueux lorsqu'on travaille sur un serveur à distance (via `ssh`) et qu'on a lancé des analyses et qu'il y a une coupure réseau.

Pour éviter ce phénomène, au lancement d'une commande on peut lui dire d'ignorer le signal SIGHUP (*hang up* /  raccroche !) :
```bash
$guest nohup sleep 1000 &
```

Si on a oublié de le faire au lancement de la commande, il est encore possible de dire au processus *via* son PID de ne pas écouter le signal (à partir du même shell/parent) :
```bash
$guest disown -h PID
```



# Valeur de retour d'un processus, structures de contrôle, scripts

Un processus retourne un entier non signé sur 8 bits (un octet) accessible dans la "variable" `$?`. Par concention, la plupart des programmes renvoie la valeur 0 pour indiquer qu'il n'y a pas eu d'erreur, et une autre valeur pour indiquer un message d'erreur (mais pas tous).

Exemples :
```bash
$guest ls -l /proc/cpuinfo 
-r--r--r-- 1 root root 0 Nov 29 17:39 /proc/cpuinfo
$guest echo $?
0

$guest ls -l /proc/nimp
ls: cannot access '/proc/nimp': No such file or directory
$guest echo $?
2
```

Cette valeur de retour permet de faire des branchements conditionnels (`if-then-else`) et donc de mettre en oeuvre des algorithmes dans scripts ou des programmes.

L'évaluation paresseuse d'une commande shell par les valeurs de retour, permet de faire ce type de branchement conditionnel, exemple :
```bash
# si le fichier/répertoire existe alors telle commande sinon telle autre
ls file.lock &> /dev/null && echo "Fichier lock trouvé" || echo "Fichier non trouvé"
ls $HOME &> /dev/null && echo "Répertoire $HOME trouvé" || echo "Répertoire $HOME non trouvé"
```



Le programme `test` permet d'évaluer des expressions et donc de faire des tests variés sur les nombres, chaînes de caractères, fichiers, ... et de combiner leur résulat de manière logique `and`/`or` :
```bash
$guest man test
$guest test -f file.lock
$guest echo $?
1
```

## `if-then-else`
```bash
if [ -f file.lock ]; 
then
  echo file.lock found
  echo "won't do anything"
else
 echo file.lock not found
 echo process will continue
fi
```

## `for` loops

`for` permet d'itérer sur une liste de valeurs :
```bash
for i in a b c; do 
  echo iteration $i
  echo   it would be nice to do something with i=$i
  echo
done
```

La liste peut provenir de l'exécution d'une autre commande ou d'une fonction :
```bash
$guest seq 4 6
4
5
6

for i in $(seq 4 6); do echo "i=$i, please do something!"; done
```






# Installation de programmes, librairies

## Installation pour tou·te·s : `dnf`, `apt`, ...

Pour installer des programmes au niveau du système, si possible, on utilise un gestionnaire : `dnf` pour les distributions RedHat, `apt` pour les distributions basées sur Debian, mais ce n'est pas obligatoire.

Ces commandes modifient le système, et il faut donc avoir des droits administrateurs pour pouvoir ajouter ou supprimer des paquets.

En U2-207 (fedora), on utilisera donc `dnf`. Il a été attribué au compte `guest` la permission d'exécuter `dnf` avec les droits administrateurs (root). Pour cela, on utilise la commande `sudo`.

`list` (consultation donc pas besoin de sudo) quand on connait le nom. Affichage de la disponibilité et de la version : 
```bash
$guest dnf list python3-*
```

`search` quand on ne connaît pas le nom du paquet. Cela a pour effet de chercher dans la description.
```bash
$guest dnf search numpy
```

`info` pour obtenir la description :
```bash
$guest dnf info python3-numpy
```

`provides` pour chercher quels paquets contiennent un fichier donné :
```bash
$guest dnf provides */libpng12.so.0.*
```


`upgrade` si il apparaît dans *Installed Packages* : rien à faire à moins qu'il y ait une version plus récente : 
```bash
$guest sudo dnf upgrade chromium
```

`install` si il apparaît dans *Available Packages* : 
```bash
$guest sudo dnf install chromium
```



## Installation pour soi : depuis le source `configure/make/install`, fichiers binaires et/ou scripts, *via* un gestionnaire d'environnement `conda/mamba`



# Gestionnaire d'environnements

Motivations :
- besoin de plusieurs versions différentes
- besoin d'un environnement stable au cours d'un projet
- tout se fait dans l'espace utilisateur : pas besoin d'avoir les droits admin
- gère les dépendances et la cohérence de l'environnement

Selon les programmes (`python`, `R`), la gestion des modules/librairies est différente (`pip/venv`, `R CMD INSTALL`). Tout ceci peut se faire en paramétrant le shell (variables d'environnement et chemins) avec `conda`. `mamba` est 100% compatible et beaucoup plus efficace.

## `mamba`

Site Web : https://github.com/mamba-org/mamba

`mamba` est déjà installé sur le compte guest. Son installation sur linux est facile : on télécharge un script qui fait l'installation et l'initialisation :

<p>
<details>
<summary><b>Installation de <tt>mamba</tt></b></summary>

Documentation : https://mamba.readthedocs.io/en/latest/installation/mamba-installation.html

Il faut donc faire la commande suivante (en tant qu'utilisateur et non **PAS root**) :
```bash
curl -L -O "https://github.com/conda-forge/miniforge/releases/latest/download/Miniforge3-$(uname)-$(uname -m).sh"
```

Puis exécuter le script téléchargé :
```bash
sh Miniforge3-$(uname)-$(uname -m).sh
```

```
Do you accept the license terms? [yes|no]
[no] >>> yes 

Mambaforge will now be installed into this location:
/home/guest/miniforge3

  - Press ENTER to confirm the location
  - Press CTRL-C to abort the installation
  - Or specify a different location below

[/home/guest/miniforge3] >>> 

Do you wish the installer to initialize Mambaforge
by running conda init? [yes|no]
[no] >>> yes
```

**Il faudra ensuite ouvrir un nouveau terminal pour voir l'effet.**


</details>
</p>


Par défaut, le répertoire qui contient tous les environnements de l'utilisateur est : `~/miniforge3`



`mamba` utilise différents `channels` pour chercher les paquets à installer. La configuration est stockée dans le fichier `~/.condarc`. En bioinfo, nous aurons besoin du channel `bioconda`. Pour l'ajouter à la configuration :
```bash
$guest cat ~/.condarc

$guest conda config --add channels bioconda

$guest cat ~/.condarc
channels:
  - bioconda
  - conda-forge
```


### toy example

```bash
$guest mamba create -n py37 python=3.7
$guest mamba create -n py39 python=3.9

$guest mamba activate py37
$guest python --version
$guest which python

$guest mamba activate py39
$guest python --version
$guest which python

$guest mamba deactivate
$guest mamba deactivate
$guest mamba remove --name py37 --all
$guest mamba remove --name py39 --all
```


## Résumé des commandes principales de conda/mamba :
```bash
# mamba configuration
mamba info


# ENVS #
########
# list environments
mamba env list

# list installed packages (in an environment)
mamba list
mamba list -n fouille

# create env with py37 and numpy
mamba create --name py37 python=3.7 numpy

# clone env
mamba create --name newly_created_env --clone existing_env

# export/import
mamba list -e > mamba.env.list.txt
mamba list --export --name fouille > fouille.mamba.list.txt

mamba create -n fouille_clone --file fouille.mamba.list.txt

# remove environment
mamba remove --name myenv --all

# activate/deactivate
mamba activate devenv
mamba deactivate

# CHANNELS #
############
conda config --add channels conda-forge 
conda config --add channels bioconda 

conda config --remove channels conda-forge

# PACKAGES #
############
mamba list
mamba search
mamba install python
mamba install python=2.7
```


## Environnement R assez complet

A l'usage, il peut être utile d'avoir un environnement rassemblant les librairies les plus fréquemment utilisées, par exemple pour des tests rapides.

On préférera (si ça marche) avoir *RStudio* (si on l'utilise) installé au niveau du système pour éviter de l'avoir séparémment dans chaque environnement → `sudo dnf install rsudio-desktop`.

Puis créer l'environnement avec ce qui nous sert le plus :
```bash
$guest mamba create --name rrstudio \
  r-highr r-knitr r-rmarkdown r-markdown r-tinytex r-xfun r-rmdformats r-devtools r-yaml r-codetools r-caTools r-rprojroot r-caret r-e1071 \
  r-tidyverse r-ggpubr r-ggally r-gridextra r-kableextra r-dt r-plotly r-rcolorbrewer r-reticulate \
  r-ade4 r-mass r-factominer r-factoextra r-uwot r-cluster
# Pour utiliser RStudio avec cette version de R et ces librairies, il faut activer l'envrionnement puis lancer rstudio dans cet environnement shell
$guest mamba activate rrstudio
$guest rstudio
```

Si on est plutôt jupyterlab plutôt que RStudio :
```bash
$guest -name rlab \
  r-highr r-knitr r-rmarkdown r-markdown r-tinytex r-xfun r-rmdformats r-devtools r-yaml r-codetools r-caTools r-rprojroot r-caret r-e1071 \
  r-tidyverse r-ggpubr r-ggally r-gridextra r-kableextra r-dt r-plotly r-rcolorbrewer r-reticulate \
  r-ade4 r-mass r-factominer r-factoextra r-uwot r-cluster \
  jupyterlab r-irkernel
$guest mamba activate rlab
$guest jupyter-lab
```

## Environnement python

Bien sûr, il nous faudra un environnement python. Par exemple avec R aussi, et jupyterlab pour passer de l'un à l'autre :
```bash
$guest mamba create -n jprlab ipykernel r-irkernel nb_conda_kernels jupyterlab \
  scipy numpy pandas scikit-learn matplotlib plotly \
  r-highr r-knitr r-rmarkdown r-markdown r-tinytex r-xfun r-rmdformats r-devtools r-yaml r-codetools r-caTools r-rprojroot r-caret r-e1071 \
  r-tidyverse r-ggpubr r-ggally r-gridextra r-kableextra r-dt r-plotly r-rcolorbrewer r-reticulate \
  r-ade4 r-mass r-factominer r-factoextra r-uwot r-cluster
$guest mamba activate jprlab
$guest rstudio
$guest jupyter-lab
```

## Environnements pour les enseignements du 2nd semestre

Fait par les M2 en septembre pour le compte guest ; se référer à https://src.koda.cnrs.fr/bioinfo/mbioinfo.workstation.setup#pour-le-m1


# Pour aller plus loin

Organisation des répertoires typique :

|  path  | comments  |
| -    | ---       |
| /      | The root directory. |
| /boot  | Boot directory (kernel and boot loader) |
| /etc   | Configuration files for the system. e.g. /etc/fstab specifies which drives to mount where. /etc/hosts lists network hosts and IP addresses. |
| /bin <br/> /usr/bin |  	The /bin directory has the essential programs that the system requires to operate, while /usr/bin contains applications for the system's users. |
| /sbin <br/> /usr/sbin | 	The sbin directories contain programs for system administration, mostly for use by the superuser (root).|
| /usr | 	contains things that support user applications. |
| /usr/local | /usr/local and its subdirectories (bin, lib, share, ...) are used for the installation of software and other files for use on the local machine i.e., not part of the official distribution. |
| /var | 	contains files that change as the system is running. This includes: log  (logs!), spool (files that are queued for some process, such as mail messages and print jobs) |
| /lib <br/> /lib64 | 	shared libraries (similar to DLLs of windows) |
| /home | 	users personal directories |
| /root | 	System administrator's home directory |
| /tmp  |	holds temporary files (anybody/program can write) |
| /dev  | 	In linux, devices are represented by files under that directory (e.g. disks are block devices such as /dev/sda or /dev/hda usually for the 1st hard drive) |
| /proc | 	virtual directory giving access to the running kernel and system. e.g. /proc/cpuinfo /proc/meminfo /proc/uptime
| /media <br/> /run/media <br/> /mnt | 	removable devices (usb sticks, usb drives, ...) are usually mounted in one of those when plugged |


Commandes :
- paths&files: cd, mkdir, rmdir, touch, ln, mv, stat, file, find
- rows&cols: cut, grep, columns, join, sort, wc
- net: ip, curl, dig, telnet, wget, ssh, sshfs
- schedule: cron, at
- version: git, diff, meld
- sed, awk
- filesystem: mount, findmnt, lsblk, fdisk, gdisk, parted, gparted, lsof, mkfs
- hw&device: inxi, lshw, lsusb, lspci
- monitor: ps, top, glance, htop, iotop, atop, iostat, iftop, netstat, nethogs


Quelques liens :
- Config U2-207 Atelier système des M2 : https://src.koda.cnrs.fr/bioinfo/mbioinfo.workstation.setup
  - section pour les M1 : https://src.koda.cnrs.fr/bioinfo/mbioinfo.workstation.setup#pour-le-m1
- Accès ssh depuis l'extérieur : https://www.univ-tlse3.fr/catalogue-de-services/acces-reseau-distant-vpn

- Containers docker/podman : https://src.koda.cnrs.fr/roland.barriot/mbioinfo.workstation.setup/-/blob/main/containers/
- https://github.com/jlevy/the-art-of-command-line

