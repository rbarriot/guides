[[_TOC_]]

----

# Liens

Je vous conseille très fortement d'au moins parcourir ces pages ci-dessous :

* **Intro git en français pour débutants** https://blog.jetpulp.fr/bases-de-git-debutant/
*  **What is Git?** https://opensource.com/resources/what-is-git
*  Getting started with Git https://opensource.com/life/16/7/stumbling-git
*  **Using Git for collaboration** https://git-scm.com/docs/gittutorial#_using_git_for_collaboration

D'autres pages qui peuvent être utiles :

*  Make Git easy with Git Cola https://opensource.com/article/20/3/git-cola

Des **éditeurs pour markdown** (Typora est relativement simple et efficace)

* https://itsfoss.com/best-markdown-editors-linux/

Pour visualiser les **différences entre 2 fichiers** textes (et éventuellement fusionner les modifications) :

* `diff`: commande linux (un peu trop basique mais utile). L'option `diff -Y file1 file2` permet de voir côté à côte les 2 fichiers mais si les lignes sont trop longues, on ne voit pas tout
* [meld](https://meldmerge.org/) ou [diffuse](http://diffuse.sourceforge.net/) : la même chose que `diff -Y` mais avec la possibilité d'appliquer les modifications

# git

## configuration/setup

Définir son compte (notamment pour pouvoir envoyer les modifs sur gitlab) :

    git config --global user.name "John Doe"
    git config --global user.email johndoe@example.com

L'éditeur de texte (pour par exemple rédiger le texte allant avec un commit) :

    git config --global core.editor vi

Pour utiliser la couleur en ligne de commande :

    git config --global color.ui true

## Démarrage : cloner un projet

Cloner le projet sans utilisation de compte gitlab :

    git clone https://gitlab.com/rbarriot/graph.git

Mieux : cloner mon projet en utilisant son compte gitlab :

    git clone git@gitlab.com:rbarriot/graph.git
  
L'URL `git@gitlab.com:rbarriot/graph.git` est fournie sur la page du projet en cliquant sur *Clone* dans *Clone with SSH*.

A partir de ce clone, vous pouvez apporter des modifications sur votre dépôt local. Il faudra ensuite les publier sur un dépôt sur lequel vous avez le droit d'écrire (ce qui n'est pas le cas pour le mien).

D'où l'intérêt depuis votre compte gitlab d'importer mon projet comme indiqué sur la page d'accueil. Admettons que votre compte soit `stubbs` et que vous avez créer un projet en important mon dépôt et que vous l'avez appelé `projet.graph`, vous pouvez le récupérer en local sur votre machine :

     git clone git@gitlab.com:stubbs/projet.graph.git

Ceci va donc télécharger la dernière version de votre projet de gitlab vers le répertoire où vous avez lancé la commande. Et vous permettre de faire toutes les modifications et de travailler sur votre machine.

Dans la section suivante, on prend l'exemple de l'ajout d'un script qui affiche quelques statistiques sur les données.

## ajout/modification de fichiers

Prenons l'exemple de l'ajout d'un script `go-obo.info.py` qui affiche des informations sur le graphe de la Gene Ontology une fois chargé.

```python
#!/bin/env python

import sys
from os.path import exists
import GeneOntology as go

obo = 'go-basic.obo'
if len(sys.argv)>1:
    obo = sys.argv[1]

if exists(obo):
    print(f"Loading OBO file '{obo}'")
    g = go.load_OBO(obo)
    nb_nodes = len(g['nodes'])
    print(f'number of GOTerms: {nb_nodes}')
    nb_edges = 0
    for u in g['nodes']: 
        for v in g['edges'][u]: nb_edges+=1
    print(f'number of relationships: {nb_edges}')
else:
    print(f"OBO file '{obo}' not found")
```

Une fois le fichier créé, il faut l'ajouter au suivi de modification. Dans le répertoire racine du projet :

    git add go-obo.info.py

## Fonctionnalités principales de git

Pour voir l'**état du dépôt et des modifications** depuis le dernier commit :

    git status

Pour voir les **modifications effectuées** :

    git diff

`diff` est un utilitaire qui permet de visualiser les différences entre fichiers textes. Il est très utilisé notamment pour les *patch* qui permettent de modifier certaines parties de fichiers. Vous devirez pouvoir trouver tout un tas de documentation sur le sujet.

**Liste des commit** effectués

    git log

La même liste sous forme de graphe

    git log --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --abbrev-commit --date=relative

### commit

La fonction **commit** permet de valider toutes les modifications apportées depuis la création ou le dernier commit. Cela créé comme une nouvelle version ou *release* du projet local. Avant de faire le commit, il est bon de vérifier ce qui va être validé (pour éventualement ajouter des fichiers nouveaux) avec `git status` (et éventuellement `git add fichiers_ou_répertoires`).

    git status
    git add go-obo.info.py
    git commit -m 'Ajout script go-obo.info.py'

### pull (récupérer une version)

Si depuis que vous avez cloné le projet (avec `git clone ...`), des modifications ont été apportées au dépôt sur gitlab (on va dire la branche master), il est possibles de récupérer les dernières modifications avec un *pull* :

    git pull origin

### push (publier une version)

Après un commit, pour partager vos modifications avec l'autre membre de l'équipe, et les publier sur le dépôt gitlab, il faut faire un *push* :

    git push origin

**Remarque :** ceci ne marche que si vous avez récupéré le projet avec git clone `git@gitlab...` (avec votre compte gitlab) et que vous avez le droit d'écrire sur le projet.

