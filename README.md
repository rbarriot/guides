# guides

Une collection de tutos, prise en main, aide mémoire :

  * [git](./git): utilisation de git et gitlab
  * [shell](./shell): tuto shell `bash` et environnements `mamba`
  * [conda](./conda): pour gérer différents environnements de développement
  * [encfs](./encfs): pour gérer la confidentialité de ses fichiers via un le cryptage/décryptage d'un répertoire 

