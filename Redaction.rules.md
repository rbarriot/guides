# Règles et conseils pour la rédaction

  * **Noms latins :** en italique (exemple : *a priori*, *etc.*). Pour le nom latin des organismes (genre espèce), le nom est fourni en entier en italique à la première utilisation, par exemple *Saccharomyces cerevisiae*, puis avec le genre en abrégé aux utilisations suivantes, par exemple *S. cerevisiae*.
  * **Ponctuation :** https://www.francaisfacile.com/exercices/exercice-francais-2/exercice-francais-115768.php
  * **Numérotation** des pages
  * **Structure :** texte en sections et sous-sections. Si elles sont nombreuses ou que le nombre de pages est important, il est conseillé d'ajouter une table des matières (avec les pages).
  * **Références :** citer les sources des figures, des informations, ...
  * **Lisibilité :** à l'impression (notamment pour les figures).
  * **Figures :** avec numérotation (pour pouvoir y faire référence dans le texte) et légende. 


**Remarque :** les règles de ponctuation sont différentes en anglais.
