# encfs

`encfs` est un programme qui permet de gérer des répertoires encryptés. Tout fichier placé dans un répertoire encrypté est encrypté de manière transparente pour l'utilisateur.

## Installation

Sous Fedora
```
sudo dnf install encfs
```

## Création/initialisation d'un répertoire

Il s'agit dans un premier temps de **créer le répertoire qui sera encrypté**. Exemple avec le réperoire que l'on appelera `charabia` :

```
mkdir -p ~/tmp/charabia
```

Pour accéder au contenu décrypté, il faut *monter* ce répertoire sur un autre que l'on appelera dans cet exemple `confidentiel` :

```
mkdir -p ~/tmp/confidentiel
```

C'est à la première utilisation, c'est-à-dire au premier montage, que le mot de passe pour créer le volume est demandé/initialisé. Premier montage :

```
encfs ~/tmp/charabia ~/tmp/confidentiel
```

Un premier choix s'offre à nous :

```
Creating new encrypted volume.
Please choose from one of the following options:
 enter "x" for expert configuration mode,
 enter "p" for pre-configured paranoia mode,
 anything else, or an empty line will select standard mode.
?> 
```

Nous prendrons l'option `standard` donc nous appuyons sur `entrée` puis fournissons le mot de passe :.

```
Standard configuration selected.

Configuration finished.  The filesystem to be created has
the following properties:
Filesystem cipher: "ssl/aes", version 3:0:2
Filename encoding: "nameio/block", version 4:0:2
Key Size: 192 bits
Block Size: 1024 bytes
Each file contains 8 byte header with unique IV data.
Filenames encoded using IV chaining mode.
File holes passed through to ciphertext.

Now you will need to enter a password for your filesystem.
You will need to remember this password, as there is absolutely
no recovery mechanism.  However, the password can be changed
later using encfsctl.

New Encfs Password: example
Verify Encfs Password: example
```

Le répertoire `charabia` crypté est donc initialisé et monté, et accessible en version décryptée dans le répertoire `confidentiel`. Pour l'instant, les 2 répertoires sont vides. Créons un fichier dans `confidentiel` pour voir ce qu'il se passe :

```
echo "contenu confidentiel" >> confidentiel/intime.txt
```

Contenu du répertoire `confidentiel` :

```
ls -l confidentiel
total 4.0K
-rw-r--r-- 1 barriot gsi 21 Apr  4 20:05 intime.txt
```

Contenu du répertoire `charabia` :
 ```
 ls -l charabia
 total 4.0K
-rw-r--r-- 1 barriot gsi 29 Apr  4 20:05 7MFGb4Vldw0Uq1DmAoI23ckm
 ```

Il est bien apparu le fichier crypté `7MFGb4Vldw0Uq1DmAoI23ckm` correspondant au fichier `intime.txt`. On notera que même les noms de fichiers sont cryptés.

## Montage d'un répertoire crypté

Chemin d'accès au répertoire crypté : `~/tmp/charabia`

Chemin d'accès au répertoire qui contiendra le contenu décrypté : `~/tmp/confidentiel`

```
encfs ~/tmp/charabia ~/tmp/confidentiel
EncFS Password: example
```

Le répertoire est monté et accessible.

## Démontage d'un répertoire crypté

Chemin d'accès au répertoire qui contient le contenu décrypté : `~/tmp/confidentiel`

```
fusermount -u ~/tmp/confidentiel
```

Vérification du contenu :

```
ls -l ~/tmp/confidentiel
total 0
```

